/* \file drvACIA.cc
   \brief Routines of the ACIA device driver
//
//      The ACIA is an asynchronous device (requests return 
//      immediately, and an interrupt happens later on).  
//      This is a layer on top of the ACIA.
//      Two working modes are to be implemented in assignment 2:
//      a Busy Waiting mode and an Interrupt mode. The Busy Waiting
//      mode implements a synchronous IO whereas IOs are asynchronous
//      IOs are implemented in the Interrupt mode (see the Nachos
//      roadmap for further details).
//
//  Copyright (c) 1999-2000 INSA de Rennes.
//  All rights reserved.  
//  See copyright_insa.h for copyright notice and limitation 
//  of liability and disclaimer of warranty provisions.
//
*/

/* Includes */

#include "kernel/system.h"         // for the ACIA object
#include "kernel/synch.h"
#include "machine/ACIA.h"
#include "drivers/drvACIA.h"

//-------------------------------------------------------------------------
// DriverACIA::DriverACIA()
/*! Constructor.
  Initialize the ACIA driver. In the ACIA Interrupt mode, 
  initialize the reception index and semaphores and allow 
  reception and emission interrupts. 
  In the ACIA Busy Waiting mode, simply inittialize the ACIA 
  working mode and create the semaphore.
  */
//-------------------------------------------------------------------------

DriverACIA::DriverACIA()
{
	#ifndef ETUDIANTS_TP
	  printf("**** Warning: contructor of the ACIA driver not implemented yet\n");
	  exit(-1);
	#endif
	#ifdef ETUDIANTS_TP
		switch (g_cfg->ACIA) {
			ind_send = 0;
			ind_rec = 0;
			case ACIA_BUSY_WAITING:
				g_machine->acia->SetWorkingMode(BUSY_WAITING);
				send_sema = new Semaphore("sendSema",1);
				receive_sema = new Semaphore("receiveSema",1);
				break;
			case ACIA_INTERRUPT:
				g_machine->acia->SetWorkingMode(SEND_INTERRUPT|REC_INTERRUPT);
				send_sema = new Semaphore("sendSema",1);
				receive_sema = new Semaphore("receiveSema",0);
				break;
		}
	#endif
  
}

//-------------------------------------------------------------------------
// DriverACIA::TtySend(char* buff)
/*! Routine to send a message through the ACIA (Busy Waiting or Interrupt mode)
  */
//-------------------------------------------------------------------------

int DriverACIA::TtySend(char* buff)
{ 
	#ifndef ETUDIANTS_TP
	  printf("**** Warning: method Tty_Send of the ACIA driver not implemented yet\n");
	  exit(-1);
	  return 0;
	#endif
	#ifdef ETUDIANTS_TP
		switch (g_cfg->ACIA) {
			case ACIA_BUSY_WAITING:
				send_sema->P();
				ind_send = 0;
				strcpy(send_buffer,buff);
				do{
					g_machine->acia->PutChar(send_buffer[ind_send]);
					while(g_machine->acia->GetOutputStateReg() == FULL);
					ind_send++;
				}while(send_buffer[ind_send-1] != 0);
				send_sema->V();
				return ind_send-1;
				break;
			case ACIA_INTERRUPT:
				send_sema->P();
				strcpy(send_buffer,buff);
				ind_send = 1;
				g_machine->acia->SetWorkingMode(SEND_INTERRUPT|REC_INTERRUPT);
				g_machine->acia->PutChar(send_buffer[0]);
				return strlen(buff);
				break;
		}
	#endif
}

//-------------------------------------------------------------------------
// DriverACIA::TtyReceive(char* buff,int length)
/*! Routine to reveive a message through the ACIA 
//  (Busy Waiting and Interrupt mode).
  */
//-------------------------------------------------------------------------

int DriverACIA::TtyReceive(char* buff,int lg)
{
	#ifndef ETUDIANTS_TP
	  printf("**** Warning: method Tty_Receive of the ACIA driver not implemented yet\n");
	  exit(-1);
	  return 0;
	#endif
	#ifdef ETUDIANTS_TP
		switch (g_cfg->ACIA) {
			case ACIA_BUSY_WAITING:
				receive_sema->P();
				ind_rec = 0;
				while(ind_rec < lg){
					while(g_machine->acia->GetInputStateReg() == EMPTY);
					receive_buffer[ind_rec] = g_machine->acia->GetChar();
					
					if(receive_buffer[ind_rec] == 0)break;
					
					ind_rec++;
				}
				strcpy(buff,receive_buffer);
				receive_sema->V();
				return ind_rec;
				break;
			case ACIA_INTERRUPT:
				receive_sema->P();
				ind_rec = 0;
				strcpy(buff,receive_buffer);
				g_machine->acia->SetWorkingMode(SEND_INTERRUPT|REC_INTERRUPT);
				return strlen(buff);
				break;
		}
	#endif
}


//-------------------------------------------------------------------------
// DriverACIA::InterruptSend()
/*! Emission interrupt handler.
  Used in the ACIA Interrupt mode only. 
  Detects when it's the end of the message (if so, releases the send_sema semaphore), else sends the next character according to index ind_send.
  */
//-------------------------------------------------------------------------

void DriverACIA::InterruptSend()
{
	#ifndef ETUDIANTS_TP
		printf("**** Warning: send interrupt handler not implemented yet\n");
		exit(-1);
	#endif
	#ifdef ETUDIANTS_TP
		if(send_buffer[ind_send] == 0){
			g_machine->acia->PutChar(send_buffer[ind_send]);
			g_machine->acia->SetWorkingMode(REC_INTERRUPT);
			send_sema->V();
		}else{
			g_machine->acia->PutChar(send_buffer[ind_send]);
			ind_send++;
		}
	#endif
}

//-------------------------------------------------------------------------
// DriverACIA::Interrupt_receive()
/*! Reception interrupt handler.
  Used in the ACIA Interrupt mode only. Reveices a character through the ACIA. 
  Releases the receive_sema semaphore and disables reception 
  interrupts when the last character of the message is received 
  (character '\0').
  */
//-------------------------------------------------------------------------

void DriverACIA::InterruptReceive()
{
	#ifndef ETUDIANTS_TP
		printf("**** Warning: receive interrupt handler not implemented yet\n");
		exit(-1);
	#endif
	#ifdef ETUDIANTS_TP
		receive_buffer[ind_rec] = g_machine->acia->GetChar();
		ind_rec++;
		if(receive_buffer[ind_rec-1] == '\0'){
			g_machine->acia->SetWorkingMode(SEND_INTERRUPT);
			receive_sema->V();
		}
	#endif
}
