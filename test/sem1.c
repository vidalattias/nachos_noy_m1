/* hello.c
 *	Simple hello world program
 *
//  Copyright (c) 1999-2000 INSA de Rennes.
//  All rights reserved.  
//  See copyright_insa.h for copyright notice and limitation 
//  of liability and disclaimer of warranty provisions.
 */

#include "userlib/syscall.h"
#include "userlib/libnachos.h"


int
main()
{
  n_printf("** ** ** Debut du test ** ** **\n\n");
  
  n_printf("sem1\n");
  
  char* mess = "hello";
  n_printf("%i",TtySend(mess));
  

  n_printf("\n\n** ** **  Fin du test ** ** **\n");

  Halt();
  return 0;
}

/*

*/
