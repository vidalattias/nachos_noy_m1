/* testMapped.c
//  Copyright (c) 1999-2000 INSA de Rennes.
//  All rights reserved.  
//  See copyright_insa.h for copyright notice and limitation 
//  of liability and disclaimer of warranty provisions.
 */

#include "userlib/syscall.h"
#include "userlib/libnachos.h"

// Table to be sorted
#define NUM 5
int A[NUM];	

int
main()
{
  n_printf("** ** ** Debut du test ** ** **\n\n");
  
  
  Create((char *)"tab",1024);
  
  OpenFileId f = Open((char *)"tab");
  
  char buffer[1024];  
  
  n_printf("Write ->");
  int z = Write("hello", 6, f);
  n_printf("%d", z);
  n_printf("<- Write \n");
  
  Seek(0, f);
    
  n_printf("Mmap ->");
  Mmap(f, 6);
  n_printf("<- Mmap \n");
  
  n_printf("Read ->");
  z = Read(buffer, 6, f);
  n_printf("%d", z);
  n_printf("<- Read \n");
  
  n_printf(buffer);
  
  
  //int i, j, key;
	/* first initialize the array, in reverse sorted order */
	
/*
    for (i = 0; i < NUM; i++)		
        A[i] = NUM - i;
        
  
  for (i = 0; i < NUM; i++) {
      n_printf("%d ",A[i]);
    }
    n_printf("\n");
    
*/
    /* then sort! *//*
    for (j=1;j<NUM;j++) {
      key = A[j];
      i = j - 1;
      while (i>= 0 && A[i] > key) {
	A[i+1] = A[i];
	i--;
      }
      A[i+1] = key;
    }

    for (i = 0; i < NUM; i++) {
      n_printf("%d ",A[i]);
    }
    n_printf("\n");

    Write("End sort\n",9,CONSOLE_OUTPUT);
*/

  n_printf("\n\n** ** **  Fin du test ** ** **\n");

  Halt();
  return 0;
}

