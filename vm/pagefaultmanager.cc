/*! \file pagefaultmanager.cc
Routines for the page fault managerPage Fault Manager
*/
//
//  Copyright (c) 1999-2000 INSA de Rennes.
//  All rights reserved.  
//  See copyright_insa.h for copyright notice and limitation 
//  of liability and disclaimer of warranty provisions.
//

#include "kernel/thread.h"
#include "vm/swapManager.h"
#include "vm/physMem.h"
#include "vm/pagefaultmanager.h"

PageFaultManager::PageFaultManager() {
}

// PageFaultManager::~PageFaultManager()
/*! Nothing for now
*/
PageFaultManager::~PageFaultManager() {
}

// ExceptionType PageFault(uint32_t virtualPage)
/*! 	
//	This method is called by the Memory Management Unit when there is a 
//      page fault. This method loads the page from :
//      - read-only sections (text,rodata) $\Rightarrow$ executive
//        file
//      - read/write sections (data,...) $\Rightarrow$ executive
//        file (1st time only), or swap file
//      - anonymous mappings (stack/bss) $\Rightarrow$ new
//        page from the MemoryManager (1st time only), or swap file
//
//	\param virtualPage the virtual page subject to the page fault
//	  (supposed to be between 0 and the
//        size of the address space, and supposed to correspond to a
//        page mapped to something [code/data/bss/...])
//	\return the exception (generally the NO_EXCEPTION constant)
*/  
ExceptionType PageFaultManager::PageFault(uint32_t virtualPage) 
{
  #ifndef ETUDIANTS_TP
	printf("**** Warning: page fault manager is not implemented yet\n");
	exit(-1);
	return ((ExceptionType)0);
	#endif
	#ifdef ETUDIANTS_TP
	
	AddrSpace * adsp = g_current_thread->GetProcessOwner()->addrspace;
	TranslationTable * tt = adsp->translationTable;
	
	while(tt->getBitIo(virtualPage)){
		g_current_thread->Yield();
	}
	
	tt->setBitIo(virtualPage);
	
	int pp = g_physical_mem_manager->AddPhysicalToVirtualMapping(g_current_thread->GetProcessOwner()->addrspace,virtualPage);
	if (pp == -1) { 
      printf("Not enough free space to load stack\n");
      g_machine->interrupt->Halt(-1);
    }
    tt->setPhysicalPage(virtualPage,pp);
	
	if(tt->getBitSwap(virtualPage)){
		while (tt->getAddrDisk(virtualPage) == -1){
			g_current_thread->Yield();
		}
		
		g_swap_manager->GetPageSwap(tt->getAddrDisk(virtualPage),
		(char *)&(g_machine->mainMemory[pp*g_cfg->PageSize]));
	}
	else{
		if(tt->getAddrDisk(virtualPage) == -1){
			memset(&(g_machine->mainMemory[pp*g_cfg->PageSize]),0x0,g_cfg->PageSize);
		}else{
			OpenFile *of = adsp->findMappedFile(virtualPage);
			if(of!=NULL){
				of->ReadAt(
					(char *)&(g_machine->mainMemory[pp*g_cfg->PageSize]),
					g_cfg->PageSize,
					tt->getAddrDisk(virtualPage));
			}else{
				g_current_thread->GetProcessOwner()->exec_file->ReadAt(
					(char *)&(g_machine->mainMemory[pp*g_cfg->PageSize]),
					g_cfg->PageSize, 
					tt->getAddrDisk(virtualPage));
			}			
		}
	}
	
	g_physical_mem_manager->UnlockPage(pp);
	tt->setBitValid(virtualPage);
	
	tt->clearBitIo(virtualPage);
	  
	return ((ExceptionType)0);
	#endif
}




